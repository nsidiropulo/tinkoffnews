//
//  ArticlesViewController.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleRepository.h"
#import "UIViewController+ShowErrorAlert.h"
#import "ArticleCell.h"

@interface ArticlesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (strong, nonatomic) ArticleRepository *articleRepository;
@property (strong, nonatomic) NSArray *articles;

@end
