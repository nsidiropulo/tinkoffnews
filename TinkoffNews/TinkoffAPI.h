//
//  TinkoffAPI.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RemoteError.h"

@interface TinkoffAPI : NSObject

@property (strong, nonatomic) NSString *rootUrl;
@property (strong, nonatomic) NSURLSession *session;

- (void)get: (NSString *)path params: (NSDictionary *)params completion: (void (^)(NSDictionary *json, RemoteError remoteError))completion;

@end
