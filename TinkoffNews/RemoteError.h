//
//  RemoteError.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

typedef enum RemoteError {
  RemoteErrorBadRequest,
  RemoteErrorBadResponse,
  RemoteErrorNoReponse,
  RemoteErrorNone
} RemoteError;
