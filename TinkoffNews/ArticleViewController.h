//
//  ArticleViewController.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article+CoreDataProperties.h"
#import "ArticleRepository.h"

@interface ArticleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *publishedLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (strong, nonatomic) ArticleRepository *articleRepository;
@property (strong, nonatomic) Article *article;

@end
