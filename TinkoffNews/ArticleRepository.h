//
//  ArticleRepository.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "AppDelegate.h"
#import "ArticleService.h"
#import "Article+CoreDataProperties.h"


@interface ArticleRepository : NSObject

@property (strong) NSManagedObjectContext *mainContext;
@property (strong) NSManagedObjectContext *syncContext;


@property (strong, nonatomic) ArticleService *service;

- (NSArray *)allArticles;
- (void)syncArticlesWithCompletion:(void (^)(NSArray *,RemoteError))completion;
- (NSArray *)allArticlesWithSyncCompletion:(void (^)(NSArray *,RemoteError))completion;

- (void)syncContentForArticle: (Article *)article completion: (void (^)(RemoteError remoteError))completion;

@end
