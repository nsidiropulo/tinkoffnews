//
//  Article+CoreDataProperties.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "Article+CoreDataProperties.h"

@implementation Article (CoreDataProperties)

+ (NSFetchRequest<Article *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Article"];
}

@dynamic title;
@dynamic content;
@dynamic published;
@dynamic serverId;

@end
