//
//  ArticleService.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "ArticleService.h"

@implementation ArticleService

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.api = [TinkoffAPI new];
  }
  return self;
}


-(void)getAllArticles:(void (^)(NSArray *, RemoteError))completion {
  [self.api get:@"news" params:nil completion:^(NSDictionary *json, RemoteError remoteError) {
    
    if (remoteError == RemoteErrorNone) {
      completion(json[@"payload"], remoteError);
    } else {
      completion(nil, remoteError);
    }
    
  }];
}

-(void)getArticleById:(NSString *)serverId completion:(void (^)(NSDictionary *, RemoteError))completion {
  [self.api get:@"news_content" params:@{@"id": serverId} completion:^(NSDictionary *json, RemoteError remoteError) {
    
    if (remoteError == RemoteErrorNone) {
      completion(json[@"payload"], remoteError);
    } else {
      completion(nil, remoteError);
    }
    
  }];
}

@end
