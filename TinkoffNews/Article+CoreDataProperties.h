//
//  Article+CoreDataProperties.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "Article+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Article (CoreDataProperties)

+ (NSFetchRequest<Article *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) NSData *content;
@property (nullable, nonatomic, copy) NSDate *published;
@property (nullable, nonatomic, copy) NSString *serverId;

@end

NS_ASSUME_NONNULL_END
