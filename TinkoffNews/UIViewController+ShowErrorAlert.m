//
//  UIViewController+ShowErrorAlert.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "UIViewController+ShowErrorAlert.h"

@implementation UIViewController (ShowErrorAlert)

-(void)showErrorAlertWithMessage:(NSString *)message {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ошибка" message:message preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *dismissAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
  [alertController addAction:dismissAction];
  [self presentViewController:alertController animated:true completion:nil];
}

-(void)showErrorAlertWithMessage:(NSString *)message afterDelay:(NSTimeInterval)delay {
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self showErrorAlertWithMessage:message];
  });
}

-(void)showAlertForRemoteError:(RemoteError)remoteError {
  
  [self showErrorAlertWithMessage:[self messageForError:remoteError]];
}

-(void)showAlertForRemoteError:(RemoteError)remoteError afterDelay:(NSTimeInterval)delay {
  
  [self showErrorAlertWithMessage:[self messageForError:remoteError] afterDelay: delay];
}

- (NSString *)messageForError: (RemoteError )remoteError {
  
  NSString *message;
  switch (remoteError) {
      
    case RemoteErrorNoReponse:
      message = @"Ошибка при соединении с сервером. Попробуйте позже";
      break;
    case RemoteErrorBadResponse:
      message = @"Ошибка на стороне сервера. Попробуйте позже";
      break;
    case RemoteErrorBadRequest:
      message = @"Ошибка на стороне клиента. Попробуйте позже";
      break;
    case RemoteErrorNone:
      break;
  }
  return message;
}

@end
