//
//  UIViewController+ShowErrorAlert.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteError.h"

@interface UIViewController (ShowErrorAlert)

- (void)showErrorAlertWithMessage: (NSString *)message;
- (void)showErrorAlertWithMessage: (NSString *)message afterDelay: (NSTimeInterval)delay;

- (void)showAlertForRemoteError: (RemoteError )remoteError;
- (void)showAlertForRemoteError: (RemoteError )remoteError afterDelay: (NSTimeInterval)delay;;

@end
