//
//  ArticlesViewController.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "ArticlesViewController.h"
#import "ArticleViewController.h"

@interface ArticlesViewController ()

@property (strong, nonatomic) NSDateFormatter *cellDateFormatter;

@end

@implementation ArticlesViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self setup];
  
  self.articles = [self.articleRepository allArticlesWithSyncCompletion:^(NSArray *articles, RemoteError remoteError) {
    
    if (remoteError == RemoteErrorNone) {
      self.articles = articles;
      [self.tableView reloadData];
    } else {
      
      if (self.articles.count == 0) {
        [self showAlertForRemoteError: remoteError];
      }
    }
    
  }];

}

- (void)setup {
  
  self.refreshControl = [UIRefreshControl new];
  self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Загрузка новостей..."];
  [self.refreshControl addTarget:self action:@selector(refreshControlDidChange:) forControlEvents:UIControlEventValueChanged];
  [self.tableView insertSubview:self.refreshControl atIndex: 0];
  
  self.tableView.estimatedRowHeight = self.tableView.rowHeight;
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  
  self.articleRepository = [ArticleRepository new];
  
  self.cellDateFormatter = [NSDateFormatter new];
  self.cellDateFormatter.dateFormat = @"dd MMMM yyyy";
  self.cellDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"ShowArticle"]) {
    ArticleViewController *articleController = segue.destinationViewController;
    UITableViewCell *selectedCell = sender;
    Article *selectedArticle = self.articles[[self.tableView indexPathForCell:selectedCell].row];
    articleController.article = selectedArticle;
  }
}

- (void)refreshControlDidChange: (UIRefreshControl *)refreshControl {
  
  [self.articleRepository syncArticlesWithCompletion:^(NSArray *articles, RemoteError remoteError) {
    
    [self.refreshControl endRefreshing];
    
    if (remoteError == RemoteErrorNone) {
      self.articles = articles;
      [self.tableView reloadData];
    } else {
      [self showAlertForRemoteError: remoteError afterDelay: 0.5];
    }
  }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.articles.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
  Article *article = self.articles[indexPath.row];
  
  cell.titleLabel.text = article.title;
  cell.publishedLabel.text = [self.cellDateFormatter stringFromDate:article.published];
  
  return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:true];
}


@end
