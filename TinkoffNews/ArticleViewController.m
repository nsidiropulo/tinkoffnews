//
//  ArticleViewController.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "ArticleViewController.h"

@interface ArticleViewController ()

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation ArticleViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self setup];
  
  self.titleLabel.text = self.article.title;
  self.publishedLabel.text = [self.dateFormatter stringFromDate:self.article.published];
  if (self.article.content != nil) {
    
    [self loadContentIntoTextViewAsync];
  } else {
    
    self.articleRepository = [ArticleRepository new];
    [self.articleRepository syncContentForArticle:self.article completion:^(RemoteError remoteError) {
      
      switch (remoteError) {
        case RemoteErrorNone:
          [self loadContentIntoTextViewAsync];
          break;
        case RemoteErrorNoReponse:
          self.contentTextView.text = @"Ошибка при соединении с сервером. Попробуйте позже";
          break;
        case RemoteErrorBadResponse:
          self.contentTextView.text = @"Ошибка на стороне сервера. Попробуйте позже";
          break;
        case RemoteErrorBadRequest:
          self.contentTextView.text = @"Ошибка на стороне клиента. Попробуйте позже";
          break;
      }
    }];
  }
  
}

- (void)setup {

  self.dateFormatter = [NSDateFormatter new];
  self.dateFormatter.dateFormat = @"Опубликовано: dd.mm.yyyy";
  self.dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
}

- (void)loadContentIntoTextViewAsync {
  
  NSData *articleContent = [NSData dataWithData:self.article.content];
  
  dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
    
    NSAttributedString *contentString = [[NSAttributedString alloc] initWithData:articleContent options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
      self.contentTextView.attributedText = contentString;
    });
  });
}

@end
