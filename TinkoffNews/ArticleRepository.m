//
//  ArticleRepository.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "ArticleRepository.h"

@implementation ArticleRepository

-(instancetype)init {
  self = [super init];
  if (self) {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    _mainContext = appDelegate.managedObjectContext;
    _syncContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_syncContext setPersistentStoreCoordinator:_mainContext.persistentStoreCoordinator];
    
    _service = [ArticleService new];
  }
  return self;
}

-(NSArray *)allArticles {
  
  __block NSArray *offlineArticles;
  [self.mainContext performBlockAndWait:^{
    
    offlineArticles = [Article findAllOrderedBy:@"published" ascending:false inContext:self.mainContext];
  }];
  
  return offlineArticles;
}

- (void)syncArticlesWithCompletion:(void (^)(NSArray *, RemoteError remoteError))completion {
  
  [self.service getAllArticles:^(NSArray *articles, RemoteError remoteError) {
    
    if (remoteError != RemoteErrorNone) {
      
      completion(nil, remoteError);
      return;
    }
    
    [self.syncContext performBlock:^{
      
      for (NSDictionary *json in articles) {
        
        NSString *serverId = json[@"id"];
        
        Article *article = [Article find:serverId inContext:self.syncContext];
        if (article == nil) {
          article = [Article articleFromJSON:json inContext:self.syncContext];
        }
      }
      
      [self.syncContext save:nil];
      
      [self.mainContext performBlock:^{
        
        NSArray *syncedArticles = [Article findAllOrderedBy:@"published" ascending:false inContext:self.mainContext];
        completion(syncedArticles, RemoteErrorNone);
      }];
      
    }];
  }];
}

-(NSArray *)allArticlesWithSyncCompletion:(void (^)(NSArray *, RemoteError remoteError))completion {
  
  [self syncArticlesWithCompletion:completion];
  return [self allArticles];
}


- (void)syncContentForArticle: (Article *)article completion: (void (^)(RemoteError remoteError))completion {
  
  [self.service getArticleById:article.serverId completion:^(NSDictionary *articleDict, RemoteError remoteError) {
    
    if (remoteError == RemoteErrorNone) {
      
      [self.mainContext performBlockAndWait:^{
        
        article.content = [articleDict[@"content"] dataUsingEncoding:NSUnicodeStringEncoding];
        [self.mainContext save:nil];
      }];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
      completion(remoteError);
    });
   
  }];
}

@end
