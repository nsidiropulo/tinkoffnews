//
//  TinkoffAPI.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "TinkoffAPI.h"

@implementation TinkoffAPI

-(instancetype)init {
  self = [super init];
  if (self) {
    self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    self.rootUrl = @"https://api.tinkoff.ru/v1/";
  }
  return self;
}


- (void)get: (NSString *)path params: (NSDictionary *)params completion: (void (^)(NSDictionary *json, RemoteError remoteError))completion {
  
  NSMutableString *getParams = [NSMutableString string];
  [params enumerateKeysAndObjectsWithOptions:kNilOptions usingBlock:^(NSString *key, NSString *value, BOOL *stop) {
    [getParams appendString:[NSString stringWithFormat:@"%@=%@", key, value]];
  }];
  NSString *encodedGetParams = [getParams stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLHostAllowedCharacterSet];
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", self.rootUrl, path, encodedGetParams]];
  
  if (url == nil) {
    
    completion(nil, RemoteErrorBadRequest);
    return;
  }
  
  [[self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    if (data) {
      
      NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
      if (json && [json[@"resultCode"] isEqualToString:@"OK"]) {
        completion(json, RemoteErrorNone);
      } else {
        completion(nil, RemoteErrorBadResponse);
      }
    } else {
      completion(nil, RemoteErrorNoReponse);
    }
    
    
  }] resume];
}

@end
