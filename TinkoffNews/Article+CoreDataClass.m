//
//  Article+CoreDataClass.m
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import "Article+CoreDataClass.h"
#import <UIKit/UIKit.h>

@implementation Article

+(Article *)articleFromJSON:(NSDictionary *)json inContext: (NSManagedObjectContext *)context {
  
  Article *article = [NSEntityDescription insertNewObjectForEntityForName:@"Article" inManagedObjectContext:context];
  article.serverId = json[@"id"];
  NSString *title = [[[NSAttributedString alloc]
                      initWithData:[json[@"text"]
                                    dataUsingEncoding:NSUnicodeStringEncoding]
                      options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                      documentAttributes:nil
                      error:nil] string];
  article.title = title;
  NSInteger ts = [json[@"publicationDate"][@"milliseconds"] doubleValue] / 1000;
  article.published = [NSDate dateWithTimeIntervalSince1970:ts];
  return article;
}

+(Article *)find:(NSString *)serverId inContext:(NSManagedObjectContext *)context {
  
  NSFetchRequest * request = [Article fetchRequest];
  request.predicate = [NSPredicate predicateWithFormat:@"serverId = %@", serverId];
  return [[context executeFetchRequest:request error:nil] firstObject];
}

+(NSArray *)findAllOrderedBy:(NSString *)param ascending:(BOOL)ascending inContext:(NSManagedObjectContext *)context {
  
  NSFetchRequest *fetchRequest = [Article fetchRequest];
  fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:param ascending:ascending]];
  fetchRequest.fetchBatchSize = 20;
  NSArray *syncedArticles = [context executeFetchRequest:fetchRequest error:nil];
  return syncedArticles;
}

@end
