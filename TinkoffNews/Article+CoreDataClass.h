//
//  Article+CoreDataClass.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Article : NSManagedObject

+ (Article *)articleFromJSON: (NSDictionary *)json inContext: (NSManagedObjectContext *)context;

+ (Article *)find: (NSString *)serverId inContext: (NSManagedObjectContext *)context;
+ (NSArray *)findAllOrderedBy: (NSString *)param ascending: (BOOL)ascending inContext: (NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Article+CoreDataProperties.h"
