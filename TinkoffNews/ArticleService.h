//
//  ArticleService.h
//  TinkoffNews
//
//  Created by Nikolay Sidiropulo on 29/09/2016.
//  Copyright © 2016 A|A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TinkoffAPI.h"

@interface ArticleService : NSObject

@property (strong, nonatomic) TinkoffAPI *api;

- (void)getAllArticles: (void (^)(NSArray *articles, RemoteError remoteError))completion;
- (void)getArticleById: (NSString *)serverId completion: (void (^)(NSDictionary *article, RemoteError remoteError))completion;

@end
